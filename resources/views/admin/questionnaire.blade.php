<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaire</title>
</head>
<body>
<h1>Questionnaires</h1>
<section>
    @if (isset ($surveys))

        <ul>
            @foreach ($surveys as $survey)
                <li>{{ $surveys->title }}</li>
            @endforeach
        </ul>
    @else
        <p> no surveys added yet </p>
    @endif
</section>
</body>
</html>
